# Zookeeper REST API
This repository contains endpoint to perform operation on the Apache Zookeeper.

## Endpoints
Its under development!

## Setup 
I have created an virtual enviroment for this project using link: https://gist.github.com/aliartiza75/16f0bd59991a9ab469f617c2a71191dd

Install the packages defined below for this project:

- python3+  
- pip3 # python package manager

##### Install the project specific pakages:

Follow the steps given below to install the project specific packages

```sh
$ cd ~/zookeeper-rest-api
$ pip3 install -r requirements.txt # install all packages defined in requirenments.txt file
$ pip3 freeze # to validate packeges have been installed 
```
